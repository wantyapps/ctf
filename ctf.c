#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char password[100];
	printf("Password: ");
	scanf("%s", password);
	if ( strcmp(password, "PASSWORD_FOR_WIN") == 0 ) {
		printf("Correct!\n");
		return 0;
	} else {
		printf("Incorrect.\n");
		return 1;
	};
	return 0;
}
